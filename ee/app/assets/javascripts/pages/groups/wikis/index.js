import initWikis from '~/pages/shared/wikis';
import initClonePanel from '~/clone_panel';

initWikis();
initClonePanel();
